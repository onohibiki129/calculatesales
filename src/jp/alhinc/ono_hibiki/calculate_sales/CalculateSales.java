package jp.alhinc.ono_hibiki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
class CalculateSales {
	public static void main(String[] args){
		
		HashMap<String,String>storeMap = new HashMap<String,String>();
		HashMap<String,Long>totalMap = new HashMap<String,Long>();
		
		List<String> storefileL = new ArrayList<>();
		
		BufferedReader br1 = null;
		
		//エラー処理：コマンドライン引数の有無
		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		
		String filePath = args[0];
		
		//支店定義ファイル読込み
		if(!storeInput(filePath, storeMap, totalMap, "branch.lst", "支店", "[0-9]{3}")){
			return;
		}
		
		
		//売上ファイル読み取り
		try{
			//ディレクトリの一覧表示
			File storeFile = new File(filePath);
			File[] storeList = storeFile.listFiles();
			
			//ディレクトリ内にマッチしたファイルがあれば抜粋し一覧化
						
			for(int i = 0; i < storeList.length; i++){
				if(storeList[i].isFile()){
					if(storeList[i].getName().matches("[0-9]{8}.rcd")){
						storefileL.add(storeList[i].getName());
					}
				}
			}
			
			//エラー処理：売上ファイルの連番チェック
			for(int i = 0; i < storefileL.size() - 1; i++){
				int before=Integer.parseInt(storefileL.get(i).substring(0, 8));
				int after=Integer.parseInt(storefileL.get(i + 1).substring(0, 8));
				if(!((after-before) == 1)){
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}
			
			//リストの初期化は計算後に数値が不要になってから。
			List<String> storeScoreL = null;
			String soloDate;
			
			for(int i = 0; i < storefileL.size(); i++){
				
				File storeScore = new File(filePath, storefileL.get(i));
				
				FileReader fr = new FileReader(storeScore);
				br1 = new BufferedReader(fr);
				
				//ファイル1個読み込むごとにリストを初期化
				storeScoreL = new ArrayList<>();
				
				while((soloDate = br1.readLine()) != null){
					storeScoreL.add(soloDate);
				}
				
				//エラー処理：支店ファイルが規定外
				if(storeScoreL.size() != 2){
					System.out.println(storeScore.getName() + "のフォーマットが不正です");
					return;
				}
				
				//エラー処理：売上ファイルの支店コードが支店定義ファイル内にあるかどうか
				if(!storeMap.containsKey(storeScoreL.get(0))){
					System.out.println(storeScore.getName() + "の支店コードが不正です");
					return;
				}
				
				//エラー処理：売上金に数値以外が入っていた場合
				if(!storeScoreL.get(1).matches("^[0-9]*$")){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				//売上集計メソッド
				if(!storeSum(storeMap, totalMap, storeScoreL)){
					return;
				}
			}	
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally{
			if(br1 != null){
				try{
					br1.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
		
		//ファイル出力メソッド
		if(!dataOutput(filePath, storeMap, totalMap, "branch.out")){
			return;
		}
	}
	
	//支店定義ファイル読込み------------------------------------------------------

	public static boolean storeInput(String filePath, HashMap<String, String> storeMap, HashMap<String, Long> totalMap, String inputFile, String dataName, String codeRule) {
		BufferedReader br = null;
		try{
			//支店定義ファイルを読み込む
					
			File baseData = new File(filePath, inputFile);
			
			//支店定義ファイルの有無を確認
			if(!baseData.exists()){
				System.out.println(dataName + "定義ファイルが存在しません");
				return false;
			}
			
			br = new BufferedReader(new FileReader(baseData));
			
			String baseDiv;
			while((baseDiv = br.readLine()) != null){
				
				//変数の分割
				String str = baseDiv;
				String[] soloData = str.split(",");
				
				//エラー処理：1行の要素数が2つ以外
				int soloDatas=soloData.length;
				if(soloDatas != 2){
					System.out.println(dataName + "定義ファイルのフォーマットが不正です");
					return false;
				}
				
				//エラー処理：コードが規定外
				if(!soloData[0].matches(codeRule)){
					System.out.println(dataName + "定義ファイルのフォーマットが不正です");
					return false;
				}
				
				//コードと店名をマッピング
				storeMap.put(soloData[0], soloData[1]);
				
				//コードとトータル金額をマッピング
				totalMap.put(soloData[0], 0L);
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if(br != null){
				try{
					br.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		
		return true;
	}
	
	//売上集計メソッド--------------------------------------------------------------
	
	private static boolean storeSum(HashMap<String, String> storeMap, HashMap<String, Long> totalMap, List<String> storeScoreL) {
		
		Long totalSum =totalMap.get(storeScoreL.get(0)) + Long.parseLong(storeScoreL.get(1));
		
		//エラー処理：売上金額が10桁を超えた場合
		if(Long.toString(totalSum).length() > 10){
			System.out.println("合計金額が10桁を超えました");
			return false;
		}
		
		//トータルの金額を合計のマップに入れる
		totalMap.put(storeScoreL.get(0), totalSum );
		
		return true;
	
	}
	
	//ファイル出力メソッド--------------------------------------------------------------
	
	public static boolean dataOutput(String filePath, HashMap<String, String> storeMap, HashMap<String, Long> totalMap, String outFile){
		
		BufferedWriter bw = null;	
		
		//結果をファイルに書き込む
		try{
			File totalFile = new File(filePath,outFile);
			FileWriter fw = new FileWriter(totalFile);
			bw = new BufferedWriter(fw);
			
			//拡張for文mapをループ処理
			for(Map.Entry<String, Long> entry : totalMap.entrySet()){
				String key = entry.getKey();
				
				bw.write(key + "," + storeMap.get(key) + "," + totalMap.get(key));
				
				//OS依存させないように改行コードを使う
				bw.newLine();
				
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if(bw != null){
				try{
					bw.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
		
	}
//---------------------------------------------------------------------------

}


